package Day1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

//Fetch Employee Data: select * from student
public class Demo2 {
	public static void main(String[] args) {

		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;

		String url = "jdbc:mysql://localhost:3306/fsd57";
		String query = "Select * from student";

		try {

			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection(url, "root", "root");

			stmt = con.createStatement();
			rs = stmt.executeQuery(query);

			while (rs.next()) {
				System.out.println("studentId   : " + rs.getInt(1));
				System.out.println("Name : " + rs.getString("Name"));
				System.out.println("Gender  : " + rs.getString("Gender"));
				System.out.println("Email : " + rs.getString("email"));
				System.out.println("password : " + rs.getString("password"));
				System.out.println("Course: " + rs.getString("course") );
				System.out.println("DOJ: " + rs.getString("DOJ") + "\n");
			}
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}
}
