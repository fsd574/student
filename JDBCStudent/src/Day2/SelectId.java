package Day2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class SelectId {
	public static void main(String[] args) {

		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;

		System.out.print("Enter Student Id: ");
		int StudentId = new Scanner(System.in).nextInt();
		System.out.println();
		
		String url = "jdbc:mysql://localhost:3306/fsd57";
		String query = "Select * from student where StudentId = " + StudentId;
		
		
		try {

			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection(url, "root", "root");

			stmt = con.createStatement();
			rs = stmt.executeQuery(query);

			if (rs.next()) {
			System.out.println("studentId   : " + rs.getInt(1));
			System.out.println("Name : " + rs.getString("Name"));
			System.out.println("Gender  : " + rs.getString("Gender"));
			System.out.println("Email : " + rs.getString("email"));
			System.out.println("password : " + rs.getString("password"));
			System.out.println("Course: " + rs.getString("course") );
			System.out.println("DOJ: " + rs.getString("DOJ") + "\n");
		}
		 else {
				System.out.println("Student Record Not Found!!!");
			}
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}
}
