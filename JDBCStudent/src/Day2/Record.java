package Day2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

//Insert Student Record
public class Record {
	public static void main(String[] args) {

		Connection con = null;
		Statement stmt = null;

		System.out.println("Enter Student Id, Name, Gender, Email, Password, course, DOJ");
		Scanner scan = new Scanner(System.in);
		int studentID = scan.nextInt();
		String name = scan.next();
		String gender = scan.next();
		String email = scan.next();
		String password = scan.next();
		String course = scan.next();
		String DOJ = scan.next();
		System.out.println();
		
		String url = "jdbc:mysql://localhost:3306/fsd57";
		String formattedDOJ = "2024-01-20";
		
		String insertQuery = "insert into student values " + 
		"(" + studentID + ", '" + name + "', '" + gender + "', '" + email + 
		"', '" + password + "', '" + course + "', '" + formattedDOJ + "')";
		
		try {

			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection(url, "root", "root");

			stmt = con.createStatement();
			int result = stmt.executeUpdate(insertQuery);

			if (result > 0) {
				System.out.println("Student Record Inserted");
			} else {
				System.out.println("Failed to Insert the Student Record!!!");
			}
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}
}
