package Day2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class SelectName {
    public static void main(String[] args) {

        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        System.out.print("Enter Student Name: ");
        String name = new Scanner(System.in).next();
        System.out.println();

        String url = "jdbc:mysql://localhost:3306/fsd57";
        String query = "SELECT * FROM student WHERE Name = ?";

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            con = DriverManager.getConnection(url, "root", "root");

            pstmt = con.prepareStatement(query);
            pstmt.setString(1, name);

            rs = pstmt.executeQuery();

            if (rs.next()) {
                System.out.println("Student ID   : " + rs.getInt("studentID"));
                System.out.println("Name         : " + rs.getString("Name"));
                System.out.println("Gender       : " + rs.getString("Gender"));
                System.out.println("Email        : " + rs.getString("email"));
                System.out.println("Password     : " + rs.getString("password"));
                System.out.println("Course       : " + rs.getString("course"));
                System.out.println("DOJ          : " + rs.getString("DOJ") + "\n");
            } else {
                System.out.println("Student Record Not Found!!!");
            }

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) rs.close();
                if (pstmt != null) pstmt.close();
                if (con != null) con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
