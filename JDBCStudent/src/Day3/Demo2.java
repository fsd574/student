package Day3;

import java.sql.Connection;
import java.sql.PreparedStatement;
//import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import com.db.DbConnection;

//Inserting a record
public class Demo2 {
	public static void main(String[] args) {
		
		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		
		System.out.println("Enter Student Id, Name, Gender, EmailId, Password, course, DOJ");
		Scanner scan = new Scanner(System.in);
		int studentID = scan.nextInt();
		String name = scan.next();
		String gender = scan.next();
		String email = scan.next();
		String password = scan.next();
		String course = scan.next();
		String DOJ = scan.next();
		System.out.println();
		
		String insertQuery = "insert into student values (?, ?, ?, ?, ?, ?, ?)";
		
		
		try {
			
			pst = con.prepareStatement(insertQuery);			
			pst.setInt(1, studentID);
			pst.setString(2, name);
			pst.setString(3, gender);
			pst.setString(4, email);
			pst.setString(5, password);	
			pst.setString(6, course);
			pst.setString(7, DOJ);
			int result = pst.executeUpdate();
			
			if (result > 0) {
				System.out.println("Record Inserted!!!");
			} else {
				System.out.println("Failed to Insert the Record.");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			if (con != null) {
				try {
					pst.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}		
	}
}

