package Assignaments;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import com.db.DbConnection;

public class UpdateStudent {

		public static void main(String[] args) {
			
			Connection con = DbConnection.getConnection();
			PreparedStatement pst = null;
			
			Scanner scan = new Scanner(System.in);
			System.out.print("Enter studentId   : ");
			int studentId = scan.nextInt();
			System.out.print("Enter name  : ");
			String name= scan.next();
			System.out.println();
			
			
			try {		
				String qry = "update student set name = ? where studentId = ?";
				
				pst = con.prepareStatement(qry);
				pst.setString(1, name);
				pst.setInt(2, studentId);			
				int result = pst.executeUpdate();		
				
				if (result > 0) {				
					System.out.println("Record(s) Updated Successfully!!!");
				} else {
					System.out.println("Record(s) Updation Failed!!!");
				}
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			finally {
				if (con != null) {
					try {
						pst.close();
						con.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
			
		}
	}

