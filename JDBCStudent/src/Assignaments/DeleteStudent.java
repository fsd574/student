package Assignaments;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

import com.db.DbConnection;

public class DeleteStudent {

    public static void main(String[] args) {

        Connection con = DbConnection.getConnection();
        PreparedStatement pst = null;

        Scanner scan = new Scanner(System.in);
        System.out.print("Enter studentId: ");
        int studentId = scan.nextInt();
        System.out.println();

        try {
            String qry = "DELETE FROM student WHERE studentId = ?";
            pst = con.prepareStatement(qry);
            pst.setInt(1, studentId);

            int result = pst.executeUpdate();

            if (result > 0) {
                System.out.println("Record(s) deleted Successfully!!!");
            } else {
                System.out.println("Record(s) deletion Failed!!!");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (con != null) {
                try {
                    pst.close();
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
