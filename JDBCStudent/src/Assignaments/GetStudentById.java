package Assignaments;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import com.db.DbConnection;

public class GetStudentById {

	public static void main(String[] args) {
			
			Connection con = DbConnection.getConnection();
			PreparedStatement pst = null;
			ResultSet rs = null;
			
			Scanner scan = new Scanner(System.in);
			System.out.print("Enter studentId: ");
			int empId = scan.nextInt();
			System.out.println();
			
			
			try {		
				pst = con.prepareStatement("Select * from student where studentId = ?");
				pst.setInt(1, empId);
				rs = pst.executeQuery();			
				
				if (rs.next()) {				
					System.out.println("student Details");
					System.out.println("----------------");
					System.out.println("studentId    : " + rs.getInt(1));
					System.out.println("Name  : " + rs.getString(2));
					System.out.println("Gender   : " + rs.getString(3));
					System.out.println("email  : " + rs.getString(4));
					System.out.println("password  : " + rs.getString(5));
					System.out.println("course  : " + rs.getString(6));
					System.out.println("DOJ  : " + rs.getString(7));


				} else {
					System.out.println("Student Record Not Found!!!");
				}
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			finally {
				if (con != null) {
					try {
						rs.close();
						pst.close();
						con.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
			
		}
	}