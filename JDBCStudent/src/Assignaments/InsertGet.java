package Assignaments;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import com.db.DbConnection;

// Inserting a record and displaying data
public class InsertGet {
    public static void main(String[] args) {

        Connection con = DbConnection.getConnection();
        PreparedStatement pst = null;
        ResultSet rs = null;

        System.out.println("Enter Student Id, Name, Gender, EmailId, Password, course, DOJ");
		Scanner scan = new Scanner(System.in);
		int studentID = scan.nextInt();
		String name = scan.next();
		String gender = scan.next();
		String email = scan.next();
		String password = scan.next();
		String course = scan.next();
		String DOJ = scan.next();
		System.out.println();

        String insertQuery = "INSERT INTO student VALUES (?, ?, ?, ?, ?, ?, ?)";
        String selectQuery = "SELECT * FROM student";

        try {

            // Inserting a record
            pst = con.prepareStatement(insertQuery);
            pst.setInt(1, studentID);
            pst.setString(2, name);
            pst.setString(3, gender);
            pst.setString(4, email);
            pst.setString(5, password);
            pst.setString(6, course);
            pst.setString(7, DOJ);
            int result = pst.executeUpdate();

            if (result > 0) {
                System.out.println("Record Inserted!!!");
            } else {
                System.out.println("Failed to Insert the Record.");
            }

            // Displaying data
            pst = con.prepareStatement(selectQuery);
            rs = pst.executeQuery();

            System.out.println("\nEmployee Table Data:");
            while (rs.next()) {
                System.out.println("tudent ID: " + rs.getInt("studentId") +
                                   ", Name: " + rs.getString("name") +
                                   ", Gender: " + rs.getString("gender") +
                                   ", Email: " + rs.getString("email") +
                                   ", Password: " + rs.getString("password")  +
                                   ", course: " + rs.getString("course") + 
                                   ", DOJ: " + rs.getString("DOJ")) ;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (con != null) {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    pst.close();
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

